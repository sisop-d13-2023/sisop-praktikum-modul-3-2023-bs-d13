# **Nomor 2**
***
Fajar sedang sad karena tidak lulus dalam mata kuliah Aljabar Linear. Selain itu, dia tambah sedih lagi karena bertemu matematika di kuliah Sistem Operasi seperti kalian. Karena dia tidak bisa ngoding dan tidak bisa menghitung, jadi Fajar memohon jokian kalian. Tugas Fajar adalah sebagai berikut.

## **Soal A**
---
Membuat program C dengan nama **kalian.c**, yang berisi program untuk melakukan perkalian matriks. Ukuran matriks pertama adalah 4×2 dan matriks kedua 2×5. Isi dari matriks didefinisikan di dalam code. Matriks nantinya akan berisi angka random dengan rentang pada matriks pertama adalah 1-5 (inklusif), dan rentang pada matriks kedua adalah 1-4 (inklusif). Tampilkan matriks hasil perkalian tadi ke layar.

> Pengertian inklusif : mengandung semua angka yang ada di dalam range untuk mengisi value matrix

## **Solusi**
``` c
void MatrixNum(int *matrix, int row, int col, int min_value, int max_value) {
    int i, j;
    int range = max_value - min_value + 1;

    for (i = 0; i < row; i++) {
        for (j = 0; j < col; j++) {
            *(matrix + i*col + j) = min_value + (i*col + j) % range;
        }
    }

    for (i = row * col - 1; i > 0; i--) {
        int j = rand() % (i + 1);
        int temp = *(matrix + i);
        *(matrix + i) = *(matrix + j);
        *(matrix + j) = temp;
    }
}
```
`void MatrixNum` merupakan fungsi untuk mengisi matrix dengan angka random sesuai dengan range yang diminta secara inklusif

``` c
void IndicateMatrixNum(int *matrix, int row, int col) {
    int i, j;
    for (i = 0; i < row; i++) {
        for (j = 0; j < col; j++) {
            printf("%d ", *(matrix + i*col + j));
        }
        printf("\n");
    }
}
```
`void IndicateMatrixNUm` merupakan fungsi untuk menampilkan value matrix sesuai ukuran yang dinputkan oleh user

``` c
void CountMatrix(int *matrix1, int *matrix2, int *value, int ROW1, int COL1, int ROW2, int COL2) {
    int i, j, k;
    for (i = 0; i < ROW1; i++) {
        for (j = 0; j < COL2; j++) {
            *(value + i*COL2 + j) = 0;
            for (k = 0; k < ROW2; k++) {
                *(value + i*COL2 + j) += *(matrix1 + i*COL1 + k) * *(matrix2 + k*COL2 + j);
            }
        }
    }
}
```
`void CountMatrix` sebuah fungsi yang digunakan untuk 
menghitung hasil perkalian antara matrix pertama dengan 
matriks kedua, kemudian hasilnya di store ke dalam variabel 
`value`.

``` c
int ROW1 = 4, COL1 = 2, ROW2 = 2, COL2 = 5;
    int matrix1[ROW1][COL1], matrix2[ROW2][COL2], value[ROW1][COL2];
```
Mendefinisikan matrix pertama dan matrix kedua beserta ukuran 
yang diminta.

``` C
srand(time(NULL));
```
Berfungsi untuk memastikan bahwa setiap program dijalankan, 
angka yang diinputkan ke dalam matrix selalu berbeda.

``` c
key_t SHM_KEY;
    const char *path = "/Users/laurivasy/Desktop/kuliah/SistemOperasi/praktikum_3/kalian.c"; 
    int id = 20030507;

    if ((SHM_KEY = ftok(path, id)) == -1) {
        perror("ftok");
        exit(1);
    }

    int shmid;
    int (*shm_value)[COL2];
    size_t shm_size = ROW1 * COL2 * sizeof(int);

    if ((shmid = shmget(SHM_KEY, shm_size, IPC_CREAT | 0666)) == -1) {
        perror("shmget");
        exit(1);
    }

    shm_value = shmat(shmid, NULL, 0);
    if (shm_value == (void *)-1) {
        perror("shmat");
        exit(1);
    }
```
* `key_t` Digunakan untuk mendeklarasikan variabel `SHM_KEY` dengan tipe data `key_t`. `key_t` adalah tipe data yang digunakan untuk mewakili ID yang akan digunakan untuk mengidentifikasi shared memory.
* `const char *path = "/Users/laurivasy/Desktop/kuliah/SistemOperasi/praktikum_3/kalian.c"` Mendeklarasikan variabel path sebagai string konstan yang berisi alamat file atau path yang akan digunakan untuk menciptakan kunci unik dengan menggunakan fungsi `ftok()`. Dalam contoh ini, path file adalah `"/Users/laurivasy/Desktop/kuliah/SistemOperasi/praktikum_3/kalian.c"`.
* `int id = 20030507` Mendeklarasikan variabel id dengan nilai `20030507`. Nilai ini akan digunakan bersama dengan path dalam fungsi `ftok()` untuk menciptakan ID.
* `if ((SHM_KEY = ftok(path, id)) == -1) { ... }`Menggunakan fungsi `ftok()` untuk menghasilkan ID berdasarkan path dan id. Jika hasilnya adalah `-1`, maka error, dan pesan error akan dicetak menggunakan `perror()`. Program akan keluar dari eksekusi dengan `exit(1)`.
* `int (*shm_value)[COL2]` Mendeklarasikan variabel `shm_value` sebagai pointer ke array 2 dimensi dengan jumlah kolom COL2. `shm_value` akan digunakan untuk mengakses dan memanipulasi data di dalam shared memory.
* `if ((shmid = shmget(SHM_KEY, shm_size, IPC_CREAT | 0666)) == -1) { ... }` Menggunakan fungsi `shmget()` untuk membuat atau mendapatkan shared memory berdasarkan `SHM_KEY` dan `shm_size`. Jika pembuatan atau pengambilan shared memory gagal, pesan error akan dicetak menggunakan `perror()`, dan program akan keluar dari eksekusi dengan `exit(1)`.
* `shm_value = shmat(shmid, NULL, 0)` Menggunakan fungsi `shmat()` untuk melekatkan shared memory ke alamat memori proses saat ini.

``` c
MatrixNum((int *)matrix1, ROW1, COL1, 1, 5);
MatrixNum((int *)matrix2, ROW2, COL2, 1, 4);
```
Mengisi matriks pertama (1-5) dan kedua (1-4) dengan angka random.

``` C
printf("Matriks pertama:\n");
IndicateMatrixNum((int *)matrix1, ROW1, COL1);
```
Menampilkan matriks 4x2 dan 2x5 yang sudah diinisiasikan dengan angka random.

``` C
CountMatrix((int *)matrix1, (int *)matrix2, (int *)value, ROW1, COL1, ROW2, COL2);
```
Menghitung hasil perkalian matriks pertama dengan matrix kedua dan menampilkan hasil perkaliannya.

``` c
for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            shm_value[i][j] = value[i][j];
        }
    }

    printf("\nHasil perkalian matriks:\n");
    IndicateMatrixNum((int *)value, ROW1, COL2);

```
Menyimpan hasil perkalian matriks ke dalam shared memory dan menampilkan hasil perkalian matrix

``` c
if (shmdt(shm_value) == -1) {
        perror("shmdt");
        exit(1);
    }
```
Merupakan bagian dari proses penghentian (detaching) shared memory setelah digunakan.

## **Output**
---
![output kalian.c](https://gitlab.com/sisop-d13-2023/sisop-praktikum-modul-3-2023-bs-d13/uploads/b0f342f5ac9651f98e065e23fbdf7c6a/output_kalian.png)

## **Nomor B & C**
---
**Soal B**
Buatlah program C kedua dengan nama **cinta.c**. Program ini akan mengambil 
variabel hasil perkalian matriks dari program **kalian.c** (program sebelumnya). 
Tampilkan hasil matriks tersebut ke layar. 
**(Catatan: wajib menerapkan konsep shared memory)**

**Soal C**
Setelah ditampilkan, berikutnya untuk setiap angka dari matriks tersebut, carilah 
nilai faktorialnya. Tampilkan hasilnya ke layar dengan format seperti matriks.
**Contoh:** 
**array [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], ...],**

maka:

1 2 6 24 120 720 ... ... …
**(Catatan: Wajib menerapkan thread dan multithreading dalam penghitungan faktorial)**

## **Solusi**
``` c
int ROW1 = 4, COL2 = 5;
void *thread_factorial(void *arg) {
    int n = *(int *)arg;
    unsigned long long result = 1;

    for (int i = 2; i <= n; i++) {
        result *= i;
    }

    if (n == 0) {
        printf("0 ");
    } else {
        printf("%llu ", result); // Add a space after printing the factorial value
    }

    pthread_exit(NULL);
}
```
`void *thread_factorial` Fungsi thread yang menghitung faktorial dari sebuah angka. Fungsi tersebut menerima 
argumen berupa pointer ke suatu angka, yang kemudian di-casting menjadi integer. Di dalam fungsi, dilakukan 
perulangan untuk mengalikan angka-angka mulai dari 2 hingga angka yang diberikan. Hasil faktorial disimpan 
dalam variabel `result`. Jika angka yang diberikan adalah 0, maka akan dicetak "0", jika bukan 0, maka akan 
dicetak nilai faktorialnya.

``` C
printf("Hasil perkalian matriks kalian.c:\n");
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            printf("%d ", shm_value[i][j]);
        }
        printf("\n");
    }
    printf("\n");
```
Digunakan untuk menampilkan hasil perkalian matriks yang sesuai dengan hasil perkalian 
yang ada di dalam file **kalian.c**,

``` c
pthread_t threads[ROW1 * COL2];
    int count = 0;

    printf("Hasil faktorial matriks:\n");

    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            pthread_create(&threads[count], NULL, thread_factorial, &shm_value[i][j]);
            count++;
        }
    }

    for (int i = 0; i < ROW1 * COL2; i++) {
        pthread_join(threads[i], NULL);
    }

    printf("\n");
```
Merupakan proses pembuatan dan penyelesaian thread untuk menghitung faktorial matriks. 
Setelah mendeklarasikan array `pthread_t` untuk menyimpan ID thread dan variabel `count` 
untuk menghitung jumlah thread, dilakukan perulangan untuk membuat thread menggunakan 
fungsi `pthread_create()`. Setiap elemen dalam matriks dijadikan argumen untuk fungsi 
`thread_factorial` dan thread akan dijalankan. Setelah semua thread dibuat, dilakukan 
perulangan lagi untuk menunggu setiap thread selesai dengan menggunakan fungsi 
`pthread_join()`. Setelah semua thread selesai, hasil faktorial matriks kemudian di 
`printf`.

## **Output**
---
![output cinta.c](https://gitlab.com/sisop-d13-2023/sisop-praktikum-modul-3-2023-bs-d13/uploads/56248e2a54a4697f39896599c3509f8b/output_cinta.png)

## **Soal D**
---
Buatlah program C ketiga dengan nama **sisop.c**. Pada program ini, lakukan apa yang 
telah kamu lakukan seperti pada cinta.c namun tanpa menggunakan thread dan 
multithreading. Buat dan modifikasi sedemikian rupa sehingga kamu 
bisa menunjukkan perbedaan atau perbandingan (hasil dan performa) antara program dengan 
multithread dengan yang tidak. Dokumentasikan dan sampaikan saat demo dan laporan resmi.

## **Solusi**
---
``` c
unsigned long long factorial(int n) {
    unsigned long long result = 1;
    for (int i = 2; i <= n; i++) {
        result *= i;
    }
    return result;
}
```
`unsigned long long factorial` merupakan fungsi untuk menghitung hasil faktorial untuk 
setiap hasil matrix.

``` c
printf("Hasil perkalian matriks kalian.c:\n");
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            printf("%d ", shm_value[i][j]);
        }
        printf("\n");
    }
    printf("\n");
```
Digunakan untuk menampilkan hasil perkalian matriks yang sesuai dengan hasil perkalian 
yang ada di dalam file **kalian.c**.

``` c
 printf("Hasil faktorial matriks:\n");

    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            int n = shm_value[i][j];
            if (n == 0) {
                printf("0 ");
            } else {
                printf("%llu ", factorial(n));
            }
        }
    }

    printf("\n");
```
Digunakan untuk menampilkan setiap hasil faktorial

## **Outout**
---
![output sisop.c](https://gitlab.com/sisop-d13-2023/sisop-praktikum-modul-3-2023-bs-d13/uploads/f5777734c750d18d6f52215682c0c3a1/output_sisop.png)

> ### **Perbandingan (hasil dan performa) antara program dengan multithread dengan yang tidak** 
> * Program tanpa penggunaan thread akan secara berurutan menghitung faktorial untuk setiap elemen matriks dan mencetak hasilnya sebelum melanjutkan ke elemen berikutnya. Dalam hal ini, program akan melakukan perhitungan faktorial secara berurutan, secara satu demi satu, sehingga penyelesaian tugas menjadi serial atau berurutan.
> * Setiap elemen matriks dihitung faktorialnya dalam sebuah thread terpisah, sehingga perhitungan dapat dilakukan secara bersamaan atau paralel. Dengan kata lain, beberapa thread akan bekerja secara bersamaan untuk menghitung faktorial dari elemen matriks yang berbeda secara independen. Hasil faktorial yang dihitung kemudian dicetak dalam urutan yang sesuai ketika semua thread telah menyelesaikan tugasnya.
> * ![sisop](https://gitlab.com/sisop-d13-2023/sisop-praktikum-modul-3-2023-bs-d13/uploads/cbca32adf12ce5525c12a6c09dfb6089/Screenshot_2023-05-13_at_21.45.38.png)
> * ![cinta](https://gitlab.com/sisop-d13-2023/sisop-praktikum-modul-3-2023-bs-d13/uploads/08aa43e712455a1fcb582b697c3a629a/Screenshot_2023-05-13_at_21.45.06.png)