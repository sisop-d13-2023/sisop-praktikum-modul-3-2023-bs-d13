#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>

unsigned long long factorial(int n) {
    unsigned long long result = 1;
    for (int i = 2; i <= n; i++) {
        result *= i;
    }
    return result;
}

int main() {
    const char *path = "/Users/laurivasy/Desktop/kuliah/SistemOperasi/praktikum_3/kalian.c";
    int id = 20030507;

    key_t SHM_KEY = ftok(path, id);
    if (SHM_KEY == -1) {
        perror("ftok");
        exit(1);
    }

    int ROW1 = 4, COL2 = 5;
    size_t shm_size = ROW1 * COL2 * sizeof(int);

    int shmid = shmget(SHM_KEY, shm_size, 0666);
    if (shmid == -1) {
        perror("shmget");
        exit(1);
    }

    int (*shm_value)[COL2] = shmat(shmid, NULL, 0);
    if (shm_value == (void *)-1) {
        perror("shmat");
        exit(1);
    }

    // Tampilkan hasil matriks
    printf("Hasil perkalian matriks kalian.c:\n");
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            printf("%d ", shm_value[i][j]);
        }
        printf("\n");
    }
    printf("\n");

    // Tampilkan hasil faktorial
    printf("Hasil faktorial matriks:\n");

    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            int n = shm_value[i][j];
            if (n == 0) {
                printf("0 ");
            } else {
                printf("%llu ", factorial(n));
            }
        }
    }

    printf("\n");

    // Lepaskan shared memory
    if (shmdt(shm_value) == -1) {
        perror("shmdt");
        exit(1);
    }

    return 0;
}
