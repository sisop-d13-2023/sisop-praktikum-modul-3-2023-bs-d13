 #include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>

int ROW1 = 4, COL2 = 5;
void *thread_factorial(void *arg) {
    int n = *(int *)arg;
    unsigned long long result = 1;

    for (int i = 2; i <= n; i++) {
        result *= i;
    }

    if (n == 0) {
        printf("0 ");
    } else {
        printf("%llu ", result);
    }

    pthread_exit(NULL);
}

int main() {
    key_t SHM_KEY;
    const char *path = "/Users/laurivasy/Desktop/kuliah/SistemOperasi/praktikum_3/kalian.c";
    int id = 20030507;

    if ((SHM_KEY = ftok(path, id)) == -1) {
        perror("ftok");
        exit(1);
    }

    int shmid;
    int (*shm_value)[COL2];
    size_t shm_size = ROW1 * COL2 * sizeof(int);

    if ((shmid = shmget(SHM_KEY, shm_size, 0666)) == -1) {
        perror("shmget");
        exit(1);
    }

    shm_value = shmat(shmid, NULL, 0);
    if (shm_value == (void *)-1) {
        perror("shmat");
        exit(1);
    }

    // Tampilkan hasil matriks
    printf("Hasil perkalian matriks kalian.c:\n");
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            printf("%d ", shm_value[i][j]);
        }
        printf("\n");
    }
    printf("\n");

    // Tampilkan hasil faktorial
    pthread_t threads[ROW1 * COL2];
    int count = 0;

    printf("Hasil faktorial matriks:\n");

    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            pthread_create(&threads[count], NULL, thread_factorial, &shm_value[i][j]);
            count++;
        }
    }

    for (int i = 0; i < ROW1 * COL2; i++) {
        pthread_join(threads[i], NULL);
    }

    printf("\n");

    if (shmdt(shm_value) == -1) {
        perror("shmdt");
        exit(1);
    } 

    return 0;
}
