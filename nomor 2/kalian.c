#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>

// fungsi untuk mengisi matriks dengan angka random
void MatrixNum(int *matrix, int row, int col, int min_value, int max_value) {
    int i, j;
    int range = max_value - min_value + 1;

    for (i = 0; i < row; i++) {
        for (j = 0; j < col; j++) {
            *(matrix + i*col + j) = min_value + (i*col + j) % range;
        }
    }

    for (i = row * col - 1; i > 0; i--) {
        int j = rand() % (i + 1);
        int temp = *(matrix + i);
        *(matrix + i) = *(matrix + j);
        *(matrix + j) = temp;
    }
}

// fungsi untuk menampilkan isi matriks 4x2 dan 2x5
void IndicateMatrixNum(int *matrix, int row, int col) {
    int i, j;
    for (i = 0; i < row; i++) {
        for (j = 0; j < col; j++) {
            printf("%d ", *(matrix + i*col + j));
        }
        printf("\n");
    }
}

// fungsi untuk menghitung perkalian matriks 4x2 dan 2x5
void CountMatrix(int *matrix1, int *matrix2, int *value, int ROW1, int COL1, int ROW2, int COL2) {
    int i, j, k;
    for (i = 0; i < ROW1; i++) {
        for (j = 0; j < COL2; j++) {
            *(value + i*COL2 + j) = 0;
            for (k = 0; k < ROW2; k++) {
                *(value + i*COL2 + j) += *(matrix1 + i*COL1 + k) * *(matrix2 + k*COL2 + j);
            }
        }
    }
}

int main() {
    int ROW1 = 4, COL1 = 2, ROW2 = 2, COL2 = 5;
    int matrix1[ROW1][COL1], matrix2[ROW2][COL2], value[ROW1][COL2];

    srand(time(NULL));

    // shared memory
    key_t SHM_KEY;
    const char *path = "/Users/laurivasy/Desktop/kuliah/SistemOperasi/praktikum_3/kalian.c"; 
    int id = 20030507;

    if ((SHM_KEY = ftok(path, id)) == -1) {
        perror("ftok");
        exit(1);
    }

    int shmid;
    int (*shm_value)[COL2];
    size_t shm_size = ROW1 * COL2 * sizeof(int);

    if ((shmid = shmget(SHM_KEY, shm_size, IPC_CREAT | 0666)) == -1) {
        perror("shmget");
        exit(1);
    }

    shm_value = shmat(shmid, NULL, 0);
    if (shm_value == (void *)-1) {
        perror("shmat");
        exit(1);
    }

    // isi matriks pertama (1-5) dan kedua (1-4) dengan angka random
    MatrixNum((int *)matrix1, ROW1, COL1, 1, 5);
    MatrixNum((int *)matrix2, ROW2, COL2, 1, 4);

    // menampilkan matriks 4x2 dan 2x5
    printf("Matriks pertama:\n");
    IndicateMatrixNum((int *)matrix1, ROW1, COL1);

    printf("\nMatriks kedua:\n");
    IndicateMatrixNum((int *)matrix2, ROW2, COL2);

    // menghitung hasil perkalian matriks dan menampilkan hasilnya
    CountMatrix((int *)matrix1, (int *)matrix2, (int *)value, ROW1, COL1, ROW2, COL2);

    // menyimpan hasil perkalian matriks ke shared memory
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            shm_value[i][j] = value[i][j];
        }
    }

    // menampilkan hasil perkalian matriks 
    printf("\nHasil perkalian matriks:\n");
    IndicateMatrixNum((int *)value, ROW1, COL2);

    if (shmdt(shm_value) == -1) {
        perror("shmdt");
        exit(1);
    }

    return 0;
}