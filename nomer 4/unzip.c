#include <stdio.h>
#include <stdlib.h>

int main() {
    int status;

    // Download file hehe.zip menggunakan wget
    status = system("wget 'https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download' -O hehe.zip");
    if (status == -1) {
        perror("Error saat download file menggunakan wget command");
        return 1;
    }

    // Unzip file hehe.zip
    status = system("unzip hehe.zip");
    if (status == -1) {
        perror("Error saat unzip file menggunakan unzip command");
        return 1;
    }

    return 0;
}
