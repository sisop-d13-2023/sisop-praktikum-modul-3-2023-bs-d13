## unzip.c

```c
#include <stdio.h>
#include <stdlib.h>
```

- Baris ini mencakup header yang diperlukan untuk fungsi `perror` dan `system`.

```c
int main() {
    int status;
```

- Fungsi utama yang mengatur aliran program dan mendefinisikan variabel `status` untuk menyimpan status dari perintah yang dijalankan.

```c
    status = system("wget 'https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download' -O hehe.zip");
    if (status == -1) {
        perror("Error saat download file menggunakan wget command");
        return 1;
    }
```

- Bagian ini menjalankan perintah `wget` untuk mengunduh file dari URL yang diberikan dan menyimpannya dengan nama `hehe.zip`. Jika perintah gagal, program akan mencetak pesan kesalahan dan keluar dengan kode status 1.

```c
    status = system("unzip hehe.zip");
    if (status == -1) {
        perror("Error saat unzip file menggunakan unzip command");
        return 1;
    }
```

- Bagian ini menjalankan perintah `unzip` untuk mengekstrak file `hehe.zip`. Jika perintah gagal, program akan mencetak pesan kesalahan dan keluar dengan kode status 1.

```c
    return 0;
}
```

- Fungsi utama mengembalikan 0, yang menunjukkan bahwa program telah berhasil dijalankan.

## Kompilasi

Untuk mengkompilasi program, jalankan perintah berikut:

```
gcc -o unzip unzip.c
```

## Menjalankan Program

Untuk menjalankan program, jalankan perintah berikut:

```
./unzip
```

Pastikan Anda memiliki perintah `wget` dan `unzip` terinstal pada sistem Anda. Program akan mengunduh file `hehe.zip` dari URL yang diberikan dan mengekstrak isinya ke direktori saat ini.

## categorize.c

```c
#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <pthread.h>
#include <stdbool.h>
```

- Baris ini mencakup header yang diperlukan untuk berbagai fungsi dan tipe data yang digunakan dalam program.

```c
#define MAX_EXTENSIONS 100
#define MAX_EXTENSION_LENGTH 10
#define MAX_FILE_COUNT 1000
#define FILE_BUFFER_SIZE 256
#define INPUT_DIR "files"
#define OUTPUT_DIR "categorized"
```

- Baris ini mendefinisikan konstanta untuk jumlah maksimum ekstensi, panjang ekstensi, jumlah file, ukuran buffer file, dan direktori input dan output.

```c
typedef struct {
  char name[MAX_EXTENSION_LENGTH];
  int count;
}
extension_t;
```

- Baris ini mendefinisikan struktur data `extension_t` untuk menyimpan nama ekstensi dan jumlah file dengan ekstensi tersebut.

```c
int other_count = 0;
int max_files;
extension_t * extensions;
int num_extensions;
```

- Baris ini mendefinisikan variabel global untuk menyimpan jumlah file "other", batas maksimum file, array ekstensi, dan jumlah ekstensi.

```c
int read_extensions(const char * filename, extension_t extensions[], int max_extensions) {
  ...
}
```

- Fungsi ini membaca file `extensions.txt` dan mengisi array `extensions` dengan ekstensi dan menginisialisasi hitungan file dengan 0.

```c
int read_max(const char * filename) {
  ...
}
```

- Fungsi ini membaca angka maksimum dari file `max.txt` dan mengembalikannya sebagai batas maksimum file per subdirektori.

```c
void join_path(const char * s1, const char * s2, char * out, char delimiter) {
  ...
}
```

- Fungsi ini menggabungkan dua string `s1` dan `s2` dengan pembatas yang diberikan, hasilnya disimpan dalam string `out`.

```c
void make_dir(const char * dir) {
  ...
}
```

- Fungsi ini membuat direktori baru jika belum ada.

```c
void get_file_extension(const char * filename, char * ext) {
  ...
}
```

- Fungsi ini mengembalikan ekstensi file (dalam lowercase) dari file yang diberikan.

```c
bool file_has_matching_extension(const char * filename, const extension_t extensions[], int num_extensions) {
  ...
}
```

- Fungsi ini memeriksa apakah file memiliki ekstensi yang cocok dengan salah satu ekstensi dalam daftar.

```c
void * process_file(void * arg) {
  ...
}
```

- Fungsi ini akan dijalankan oleh thread dan memproses satu file dengan mengkategorinya berdasarkan ekstensinya dan memindahkannya ke direktori output yang sesuai.

```c
int main() {
  ...
}
```

- Fungsi utama yang mengatur aliran program, membaca file ekstensi dan batas maksimum, membuat direktori output, dan membuat thread untuk memproses file.

## Kompilasi

Untuk mengkompilasi program, jalankan perintah berikut:

```
gcc -o categorize categorize.c -pthread
```

## Menjalankan Program

Untuk menjalankan program, jalankan perintah berikut:

```
./categorize
```

Pastikan Anda memiliki direktori `files` yang berisi file yang ingin dikategorikan dan file konfigurasi `extensions.txt` dan `max.txt` di direktori yang sama dengan program yang dikompilasi.

## Konfigurasi

- `extensions.txt`: Daftar ekstensi yang akan digunakan untuk mengkategorikan file. Satu ekstensi per baris.
- `max.txt`: Angka yang menentukan jumlah maksimum file dalam setiap subdirektori.
