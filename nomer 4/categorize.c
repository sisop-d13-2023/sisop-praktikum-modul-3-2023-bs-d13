#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <pthread.h>
#include <stdbool.h>

#define MAX_EXTENSIONS 100
#define MAX_EXTENSION_LENGTH 10
#define MAX_FILE_COUNT 1000
#define FILE_BUFFER_SIZE 256
#define INPUT_DIR "files"
#define OUTPUT_DIR "categorized"

typedef struct {
  char name[MAX_EXTENSION_LENGTH];
  int count;
}
extension_t;

// Variabel global untuk menyimpan jumlah file "other" dan batas maksimum file.
int other_count = 0;
int max_files;
extension_t * extensions;
int num_extensions;

// Fungsi untuk membaca file extensions.txt dan mengisi array extensions.
// Membaca setiap baris dari file dan mengisi array dengan ekstensi dan menginisialisasi hitungan file dengan 0.
int read_extensions(const char * filename, extension_t extensions[], int max_extensions) {
  FILE * file = fopen(filename, "r");
  if (!file) {
    perror("Error opening extensions file");
    return -1;
  }

  int count = 0;
  while (count < max_extensions && fscanf(file, "%s", extensions[count].name) != EOF) {
    extensions[count].count = 0;
    count++;
  }

  fclose(file);
  return count;
}

// Fungsi untuk membaca angka maksimum dari file max.txt.
// Membaca angka pertama dari file dan mengembalikannya sebagai batas maksimum file per subdirektori.
int read_max(const char * filename) {
  int max;
  FILE * file = fopen(filename, "r");
  if (!file) {
    perror("Error opening max file");
    return -1;
  }

  fscanf(file, "%d", & max);
  fclose(file);
  return max;
}

// Fungsi untuk menggabungkan dua string dengan pembatas.
// Menggabungkan dua string s1 dan s2 dengan pembatas yang diberikan, hasilnya disimpan dalam string out.
void join_path(const char * s1, const char * s2, char * out, char delimiter) {
  strcpy(out, s1);
  strcat(out, "/");
  strcat(out, s2);
}

// Fungsi untuk membuat direktori.
// Membuat direktori baru.
void make_dir(const char * dir) {
  struct stat st = {0};
  if (stat(dir, & st) == -1) {
    mkdir(dir, 0755);
  }
}

// Fungsi untuk mengembalikan file extension (dalam lowercase) dari file.
// Mengekstrak ekstensi file dari nama file yang diberikan, mengonversinya menjadi lowercase, dan menyimpannya dalam string ext.
void get_file_extension(const char * filename, char * ext) {
  const char * dot = strrchr(filename, '.');
  if (!dot || dot == filename) {
    strcpy(ext, "other");
  } else {
    strcpy(ext, dot + 1);
  }
  for (int i = 0; ext[i]; i++) {
    ext[i] = tolower(ext[i]);
  }
}

// Fungsi untuk memeriksa apakah file memiliki ekstensi yang cocok dengan salah satu ekstensi dalam daftar.
// Memeriksa apakah ekstensi file dalam daftar ekstensi yang diberikan, mengembalikan true jika ada yang cocok, false jika tidak ada yang cocok.
bool file_has_matching_extension(const char * filename, const extension_t extensions[], int num_extensions) {
  char ext[MAX_EXTENSION_LENGTH];
  get_file_extension(filename, ext);

  for (int i = 0; i < num_extensions; i++) {
    if (strcmp(ext, extensions[i].name) == 0) {
      return true;
    }
  }
  return false;
}

// Fungsi yang akan dijalankan oleh thread.
// Memproses satu file dengan mengkategorinya berdasarkan ekstensinya dan memindahkannya ke direktori output yang sesuai.
void * process_file(void * arg) {
  char * filename = (char * ) arg;
  char input_path[FILE_BUFFER_SIZE];
  char output_path[FILE_BUFFER_SIZE];
  char ext[MAX_EXTENSION_LENGTH];

  join_path(INPUT_DIR, filename, input_path, '/');
  get_file_extension(filename, ext);

// Mencari ekstensi yang cocok dalam array ekstensi.
  int ext_index = -1;
  for (int i = 0; i < num_extensions; i++) {
    if (strcmp(ext, extensions[i].name) == 0) {
      ext_index = i;
      break;
    }
  }

// Jika ditemukan ekstensi yang cocok, gunakan direktori ekstensi; jika tidak, gunakan direktori "other".
  const char * output_dir;
  if (ext_index != -1) {
    output_dir = extensions[ext_index].name;
  } else {
    output_dir = "other";
  }

  char output_dir_path[FILE_BUFFER_SIZE];
  join_path(OUTPUT_DIR, output_dir, output_dir_path, '/');
  // Membuat direktori output jika belum ada.
  make_dir(output_dir_path);

// Menentukan direktori output berdasarkan jumlah file saat ini dan batas maksimum.
  int num_files = (ext_index != -1) ? extensions[ext_index].count : other_count;
  int subdir_num = num_files / max_files + 1;
  char subdir_name[FILE_BUFFER_SIZE];
  snprintf(subdir_name, FILE_BUFFER_SIZE, "%s (%d)", output_dir, subdir_num);
  char subdir_path[FILE_BUFFER_SIZE];
  join_path(output_dir_path, subdir_name, subdir_path, '/');
  make_dir(subdir_path);

// Menggabungkan nama direktori output dan nama file, lalu memindahkan file dari direktori input ke direktori output.
  join_path(subdir_path, filename, output_path, '/');
  rename(input_path, output_path);

// Menambahkan count pada ekstensi yang sesuai.
  if (ext_index != -1) {
    extensions[ext_index].count++;
  } else {
    other_count++;
  }

  return NULL;
}

int main() {
  extension_t exts[MAX_EXTENSIONS];
  extensions = exts;
  num_extensions = read_extensions("extensions.txt", extensions, MAX_EXTENSIONS);
  max_files = read_max("max.txt");

  make_dir(OUTPUT_DIR);

  DIR * dir = opendir(INPUT_DIR);
  if (!dir) {
    perror("Error opening input directory");
    return 1;
  }

  struct dirent * entry;
  pthread_t threads[MAX_FILE_COUNT];
  int thread_count = 0;

  while ((entry = readdir(dir))) {
    if (entry -> d_type == DT_REG) {
        // Membuat thread untuk setiap file.
      pthread_create( & threads[thread_count], NULL, process_file, strdup(entry -> d_name));
      thread_count++;
    }
  }

// Menunggu semua thread selesai.
  for (int i = 0; i < thread_count; i++) {
    pthread_join(threads[i], NULL);
  }

// Menampilkan hasil.
  for (int i = 0; i < num_extensions; i++) {
    printf("%s : %d\n", extensions[i].name, extensions[i].count);
  }
  printf("other : %d\n", other_count);

  closedir(dir);
  return 0;
}
