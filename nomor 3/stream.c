#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <wait.h>
#include <semaphore.h>
#include <fcntl.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <ctype.h>
#include <unistd.h>

#define BUFFER_SIZE 1024

char songs[2000][150];
char songs_lowercase[2000][150];
sem_t *mutex;

struct mesg_buffer
{
    long mesg_type;
    char mesg_text[100];
    char cmd;
    int id;
} command;

void toLowercase(char *str)
{
    int len = strlen(str);
    for (int i = 0; i < len; i++)
    {
        str[i] = tolower(str[i]);
    }
}

void getSongList()
{
    pid_t pid = fork();
    if (!pid)
        return;

    if (sem_trywait(mutex))
    {
        printf("STREAM SYSTEM OVERLOAD");
        exit(0);
        return;
    }

    char line[BUFFER_SIZE];
    char command[20000];

    system("echo > playlist.txt");
    system("awk 'BEGIN{FS = \"\\\"\"};/method/{printf \"%s=\", $4}; /song/{print $4}' song-playlist.json > temp.txt");

    FILE *fp = fopen("temp.txt", "r");

    if (fp == NULL)
    {
        printf("Failed to open file\n");
        return;
    }

    int methodLength;
    char crypt[7];

    while (fgets(line, sizeof(line), fp))
    {
        pid_t pid = fork();
        if (!pid)
            return;

        if (sem_trywait(mutex))
        {
            printf("STREAM SYSTEM OVERLOAD");
            exit(0);
            return;
        }

        char *method;
        char *encryptedSong;

        // Remove trailing newline character
        line[strcspn(line, "\n")] = '\0';

        encryptedSong = strchr(line, '=');
        methodLength = strlen(line) - strlen(encryptedSong);
        strncpy(crypt, line, methodLength);
        crypt[methodLength] = '\0';
        encryptedSong = encryptedSong + 1;

        if (!strcmp("hex", crypt))
            sprintf(command, "%s echo \"%s\" | xxd -r -p >> songlist.txt; echo >> songlist.txt;", command, encryptedSong);
        else if (!strcmp("base64", crypt))
            sprintf(command, "%s echo \"%s\" | base64 -d >> songlist.txt; echo >> songlist.txt;", command, encryptedSong);
        else if (!strcmp("rot13", crypt))
            sprintf(command, "%s echo \"%s\" | tr 'A-Za-z' 'N-ZA-Mn-za-m' >> songlist.txt;", command, encryptedSong);

        if (strlen(command) + strlen(encryptedSong) + 28 > 19900)
        {
            system(command);
            *command = '\0';
        }
    }

    if (strlen(command))
        system(command);

    fclose(fp);

    system("cat songlist.txt | sort -f > playlist.txt");
    system("rm temp.txt; rm songlist.txt;");

    FILE *fps = fopen("songlist.txt", "r");
    int i = 0;
    while (fgets(line, sizeof(line), fps))
    {
        // Remove trailing newline character
        line[strcspn(line, "\n")] = '\0';
        strcpy(songs[i], line);
        strcpy(songs_lowercase[i], line);
        toLowercase(songs_lowercase[i]);
        i++;
    }

    fclose(fps);

    sem_post(mutex);
    exit(0);
}

void list()
{
    pid_t pid = fork();
    if (!pid)
        return;

    if (sem_trywait(mutex))
    {
        printf("STREAM SYSTEM OVERLOAD");
        exit(0);
        return;
    }

    system("cat playlist.txt");

    sem_post(mutex);
    exit(0);
}

void add(int id, char *song)
{
    pid_t pid = fork();
    if (!pid)
        return;

    if (sem_trywait(mutex) != 0)
    {
        printf("STREAM SYSTEM OVERLOAD\n");
        exit(0);
    }

    char command[200];
    sprintf(command, "awk \"BEGIN{IGNORECASE = 1; found = 0} \\$0 == \\\"%s\\\"{found = 1} END{print found}\" playlist.txt >> temp.txt", song);
    system(command);

    FILE *fp = fopen("temp.txt", "r");
    int found = 0;
    fscanf(fp, "%d", &found);
    if (found)
    {
        printf("SONG ALREADY ON PLAYLIST\n");
        system("rm temp.txt");
        fclose(fp);
        sem_post(mutex);
        exit(0);
    }
    sprintf(command, "echo '%s' >> playlist.txt", song);
    system(command);
    system("cat playlist.txt | sort -f > temp.txt");
    system("cat temp.txt > playlist.txt");
    system("rm temp.txt");
    printf("USER %d ADD %s\n", id, song);
    sem_post(mutex);
    fclose(fp);
    exit(0);
}

void play(int id, char *song)
{
    printf("test\n");
    pid_t pid = fork();
    if (!pid)
        return;

    if (sem_trywait(mutex))
    {
        printf("STREAM SYSTEM OVERLOAD");
        exit(0);
        return;
    }

    int len = strlen(song);
    for (int i = 0; i < len; i++)
        song[i] = tolower(song[i]);

    int counter = 0;
    int data[1000];

    for (int i = 0; i < 1000; i++)
    {
        if (strstr(songs_lowercase[i], song))
        {
            printf("Playing: %s", songs_lowercase[i]);
            data[++counter] = i;
        }
    }

    if (counter == 1)
        printf("USER %d PLAYING \"%s\"\n", id, songs_lowercase[data[0]]);
    else if (counter > 1)
    {
        printf("THERE ARE \"%d\" SONG CONTAINING \"%s\":", counter, song);
        for (int i = 0; i < counter; i++)
            printf("%d. %s", i, songs[i]);
    }

    sem_post(mutex);
    exit(0);
}

void executeCommand()
{
    printf("cmd: %d, userID: %d \n", command.cmd, command.id);
    switch (command.cmd)
    {
    case 1:
        getSongList();
        break;
    case 2:
        list();
        break;
    case 3:
        play(command.id, command.mesg_text);
        break;
    case 4:
        add(command.id, command.mesg_text);
        break;
    default:
        printf("UNKNOWN COMMAND\n");
        break;
    }
}

int main()
{
    mutex = sem_open("playlist", O_CREAT, 0644, 2);
    sem_init(mutex, 1, 2);

    key_t key = ftok("progfile", 65);
    int msgid = msgget(key, 0666 | IPC_CREAT);

    while (1)
    {
        msgrcv(msgid, &command, sizeof(command), 1, 0);
        executeCommand();
    }

    msgctl(msgid, IPC_RMID, NULL);

    return 0;
}
