#include <stdio.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <ctype.h>
#define MAX 100

struct mesg_buffer {
    long mesg_type;
    char mesg_text[100];
    char cmd;
    int id;
} command;

struct msgbuf {
    long mtype;    
    char mtext[1];    
};

char *ltrim(char *s)
{
    while(isspace(*s)) s++;
    return s;
}

int getId(){
    key_t key = ftok("user_id", 65); 
    int msgid = msgget(key, 0666 | IPC_CREAT);

    struct msqid_ds buf;

    int rc = msgctl(msgid, IPC_STAT, &buf);
    unsigned int user_id = (unsigned int)(buf.msg_qnum);
    struct msgbuf qmsg;
    qmsg.mtype = 100;
    qmsg.mtext[0] = 'T';
    
    msgsnd(msgid, &qmsg, 1, MSG_NOERROR);
    return user_id;
}

int main()
{
    key_t key;
    int msgid;
    char *arg, cmd[10];
  
    // ftok to generate unique key
    key = ftok("progfile", 65);  
    command.id = getId();

    int arglen;
    printf("user_id: %d\n", command.id);
  
    while(1) {
        // msgget creates a command queue
        // and returns identifier
        msgid = msgget(key, 0666 | IPC_CREAT);
        command.mesg_type = 1;
        printf("Command : ");
        fgets(command.mesg_text, MAX, stdin);
        command.mesg_text[strcspn(command.mesg_text, "\n")] = '\0';

        arg = strchr(command.mesg_text, ' ');
        
        if(arg){ 
            arglen = strlen(arg);
            strncpy(cmd, command.mesg_text, strlen(command.mesg_text) - arglen);
            cmd[strlen(command.mesg_text) - arglen] = '\0';
        }else {
            strcpy(cmd, command.mesg_text); 
            *command.mesg_text = '\0';
        }

        command.cmd = !strcasecmp(cmd, "DECRYPT")? 1 : 
                      !strcasecmp(cmd, "LIST")? 2 : 
                      !strcasecmp(cmd, "PLAY")? 3 : 
                      !strcasecmp(cmd, "ADD")? 4: -1;

        if (command.cmd >= 3 && arg) {
            if(strchr(arg, '\"')) {
                char *value = strtok(arg, "\"");
                value = strtok(NULL, "\"");
                strcpy(command.mesg_text, value);
            } else strcpy(command.mesg_text, arg);
        } else if(command.cmd >= 3) printf("Memerlukan Argument Lagu!\n");

        strcpy(command.mesg_text, ltrim(command.mesg_text));

        // msgsnd to send command
        msgsnd(msgid, &command, sizeof(command), 0);
    }
    
  
    return 0;
}