# stream.c

Stream.c berisi sistem utamanya yang mengatur banyak hal seperti Fetching data, menjalankan command dari user, menerima message berupa command dari file user.c, mengatur semaphore, dll.

## Global Variable

```c
char songs[2000][150];
char songs_lowercase[2000][150];
sem_t *mutex;
```

`songs` adalah array 2 dimensi yang memiliki 1000 data dan berisi string / char[150] yang merupakan data lagu lagu. \
`songs_lowercase` adalah sama dengan songs tetapi semua judul berupa lowercase. \
`mutex` adalah variable untuk menghandle semaphore yang akan digunakan.

## mesg_buffer()

```c
struct mesg_buffer {
    long mesg_type;
    char mesg_text[100];
    char cmd;
    int id;
} command;
```

struct `mesg_buffer` hanya di gunakan sekali dan langsung digunakan untuk deklarasi vairable command. Dipakai untuk handle pesan yang dikirim dari program user.c

## getSongList()

```c
void getSongList()
{
    pid_t pid = fork();
    if (!pid)
        return;

    if (sem_trywait(mutex))
    {
        printf("STREAM SYSTEM OVERLOAD");
        exit(0);
        return;
    }

    char line[BUFFER_SIZE];
    char command[20000];

    system("echo > playlist.txt");
    system("awk 'BEGIN{FS = \"\\\"\"};/method/{printf \"%s=\", $4}; /song/{print $4}' song-playlist.json > temp.txt");

    FILE *fp = fopen("temp.txt", "r");

    if (fp == NULL)
    {
        printf("Failed to open file\n");
        return;
    }

    int methodLength;
    char crypt[7];

    while (fgets(line, sizeof(line), fp))
    {

        char *method;
        char *encryptedSong;

        // Remove trailing newline character
        line[strcspn(line, "\n")] = '\0';

        encryptedSong = strchr(line, '=');
        methodLength = strlen(line) - strlen(encryptedSong);
        strncpy(crypt, line, methodLength);
        crypt[methodLength] = '\0';
        encryptedSong = encryptedSong + 1;

        if (!strcmp("hex", crypt))
            sprintf(command, "%s echo \"%s\" | xxd -r -p >> songlist.txt; echo >> songlist.txt;", command, encryptedSong);
        else if (!strcmp("base64", crypt))
            sprintf(command, "%s echo \"%s\" | base64 -d >> songlist.txt; echo >> songlist.txt;", command, encryptedSong);
        else if (!strcmp("rot13", crypt))
            sprintf(command, "%s echo \"%s\" | tr 'A-Za-z' 'N-ZA-Mn-za-m' >> songlist.txt;", command, encryptedSong);

        if (strlen(command) + strlen(encryptedSong) + 28 > 19900)
        {
            system(command);
            *command = '\0';
        }
    }

    if (strlen(command))
        system(command);

    fclose(fp);

    system("cat songlist.txt | sort -f > playlist.txt");
    system("rm temp.txt; rm songlist.txt;");

    FILE *fps = fopen("songlist.txt", "r");
    int i = 0;
    while (fgets(line, sizeof(line), fps))
    {
        // Remove trailing newline character
        line[strcspn(line, "\n")] = '\0';
        strcpy(songs[i], line);
        strcpy(songs_lowercase[i], line);
        toLowercase(songs_lowercase[i]);
        i++;
    }

    fclose(fps);

    sem_post(mutex);
    exit(0);
}
```

fungsi ini digunakan untuk menjalankan command "DECRYPT". Pertama tama buat child proses dan lalu check semaphore, jika semaphore penuh maka akan exit child proses. \

```c
system("awk 'BEGIN{FS = \"\\\"\"};/method/{printf \"%s=\", $4}; /song/{print $4}' song-playlist.json > temp.txt");
```

dengan script AWK ini akan mengecheck apakah suatu suatu line mengandung kata 'method' atau 'song' jika ada method maka print tanpa new line + '=', dan jika ketemu song maka akan print dengan newline. \
maka result perline akan kira kira seperti ini

```c
{method}={song name}
```

Lalu akan di save ke temp.txt, lalu temp.txt tersebut akan dibaca perline, dan akan di dekripsi sesuai dengan method nya. Command akan di store kedalam 1 string dan akan di jalankan bersama sama, kira kira ada 3 batch untuk 999 song name. lalu hasil nya akan di store kedalam variable array songs.

## list()

```c
void list()
{
    pid_t pid = fork();
    if (!pid)
        return;

    if (sem_trywait(mutex))
    {
        printf("STREAM SYSTEM OVERLOAD");
        exit(0);
        return;
    }

    system("cat playlist.txt");

    sem_post(mutex);
    exit(0);
}
```

list sesimple menampilkan isi dari playlist.txt dengan metode cat.

## add()

```c
void add(int id, char *song)
{
    pid_t pid = fork();
    if (!pid)
        return;

    if (sem_trywait(mutex) != 0)
    {
        printf("STREAM SYSTEM OVERLOAD\n");
        exit(0);
    }

    char command[200];
    sprintf(command, "awk \"BEGIN{IGNORECASE = 1; found = 0} \\$0 == \\\"%s\\\"{found = 1} END{print found}\" playlist.txt >> temp.txt", song);
    system(command);

    FILE *fp = fopen("temp.txt", "r");
    int found = 0;
    fscanf(fp, "%d", &found);
    if (found)
    {
        printf("SONG ALREADY ON PLAYLIST\n");
        system("rm temp.txt");
        fclose(fp);
        sem_post(mutex);
        exit(0);
    }
    sprintf(command, "echo '%s' >> playlist.txt", song);
    system(command);
    system("cat playlist.txt | sort -f > temp.txt");
    system("cat temp.txt > playlist.txt");
    system("rm temp.txt");
    printf("USER %d ADD %s\n", id, song);
    sem_post(mutex);
    fclose(fp);
    exit(0);
}
```

fungsi add() di panggil ketika nama lagu yang di inputkan sudah ada maka tidak akan ditambahin lagi ke dalam, tetapi jika belum ada maka akan di inputkan ke dalam file playlist.txt

# play()

```c
void play(int id, char *song)
{
    printf("test\n");
    pid_t pid = fork();
    if (!pid)
        return;

    if (sem_trywait(mutex))
    {
        printf("STREAM SYSTEM OVERLOAD");
        exit(0);
        return;
    }

    int len = strlen(song);
    for (int i = 0; i < len; i++)
        song[i] = tolower(song[i]);

    int counter = 0;
    int data[1000];

    for (int i = 0; i < 1000; i++)
    {
        if (strstr(songs_lowercase[i], song))
        {
            printf("Playing: %s", songs_lowercase[i]);
            data[++counter] = i;
        }
    }

    if (counter == 1)
        printf("USER %d PLAYING \"%s\"\n", id, songs_lowercase[data[0]]);
    else if (counter > 1)
    {
        printf("THERE ARE \"%d\" SONG CONTAINING \"%s\":", counter, song);
        for (int i = 0; i < counter; i++)
            printf("%d. %s", i, songs[i]);
    }

    sem_post(mutex);
    exit(0);
}
```

play akan mengecheck jika ada yang mengandung kata commandnya.

## executeCommand()

```c
void executeCommand()
{
    printf("cmd: %d, userID: %d \n", command.cmd, command.id);
    switch (command.cmd)
    {
    case 1:
        getSongList();
        break;
    case 2:
        list();
        break;
    case 3:
        play(command.id, command.mesg_text);
        break;
    case 4:
        add(command.id, command.mesg_text);
        break;
    default:
        printf("UNKNOWN COMMAND\n");
        break;
    }
}
```

fungsi ini khusus untuk eksekusi command yang di dapat dari msg queue.

## MAIN

```c
int main()
{
    mutex = sem_open("playlist", O_CREAT, 0644, 2);
    sem_init(mutex, 1, 2);

    key_t key = ftok("progfile", 65);
    int msgid = msgget(key, 0666 | IPC_CREAT);

    while (1)
    {
        msgrcv(msgid, &command, sizeof(command), 1, 0);
        executeCommand();
    }

    msgctl(msgid, IPC_RMID, NULL);

    return 0;
}
```

fungsi main ini ada `sem_open()` untuk membuka semaphore dan `sem_init()` untuk inisiasi semaphore dan menginisiasi 2 semaphore. lalu mengeset msg id nya, lalu stream / mendengarkan msgrcv. ketika ada message dalam msg queue maka akan dijalankan `executeCommand()`.

# user.c

User.c adalah program khusus untuk user dan mengirimkan command ke dalam msg queue. Hanya ada 1 function yang tidak begitu esensial.

## getId()

```c
int getId(){
    key_t key = ftok("user_id", 65);
    int msgid = msgget(key, 0666 | IPC_CREAT);

    struct msqid_ds buf;

    int rc = msgctl(msgid, IPC_STAT, &buf);
    unsigned int user_id = (unsigned int)(buf.msg_qnum);
    struct msgbuf qmsg;
    qmsg.mtype = 100;
    qmsg.mtext[0] = 'T';

    msgsnd(msgid, &qmsg, 1, MSG_NOERROR);
    return user_id;
}
```

fungsi ini akan mengirimkan message kosong ke dalam msg queue dan akan menghitung berapa banyak queue yang ada di dalam msg queue. Lalu jumlah queue itu akan dijadikan userId. dan akan selalu incerement selama PC belum di restart atau queue belum reset.

## MAIN

```c
int main()
{
    key_t key;
    int msgid;
    char *arg, cmd[10];

    // ftok to generate unique key
    key = ftok("progfile", 65);
    command.id = getId();

    int arglen;
    printf("user_id: %d\n", command.id);

    while(1) {
        // msgget creates a command queue
        // and returns identifier
        msgid = msgget(key, 0666 | IPC_CREAT);
        command.mesg_type = 1;
        printf("Command : ");
        fgets(command.mesg_text, MAX, stdin);
        command.mesg_text[strcspn(command.mesg_text, "\n")] = '\0';

        arg = strchr(command.mesg_text, ' ');

        if(arg){
            arglen = strlen(arg);
            strncpy(cmd, command.mesg_text, strlen(command.mesg_text) - arglen);
            cmd[strlen(command.mesg_text) - arglen] = '\0';
        }else {
            strcpy(cmd, command.mesg_text);
            *command.mesg_text = '\0';
        }

        command.cmd = !strcasecmp(cmd, "DECRYPT")? 1 :
                      !strcasecmp(cmd, "LIST")? 2 :
                      !strcasecmp(cmd, "PLAY")? 3 :
                      !strcasecmp(cmd, "ADD")? 4: -1;

        if (command.cmd >= 3 && arg) {
            if(strchr(arg, '\"')) {
                char *value = strtok(arg, "\"");
                value = strtok(NULL, "\"");
                strcpy(command.mesg_text, value);
            } else strcpy(command.mesg_text, arg);
        } else if(command.cmd >= 3) printf("Memerlukan Argument Lagu!\n");

        strcpy(command.mesg_text, ltrim(command.mesg_text));

        // msgsnd to send command
        msgsnd(msgid, &command, sizeof(command), 0);
    }


    return 0;
}
```

Pertama akan mengisiasi user id dengan memanggil `getId()`. Lalu dengan while akan menunggu input command dari user, ketika user memasukkan command, command akan di fetch lalu di masukkan dalam variable command, lalu variable tersebut akan di kirim ke message queue.
