#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h> 
#include <sys/types.h> 
#include <string.h> 
#include <sys/wait.h> 
#define MAX_TREE_HT 50
#define MAXCHAR 1000

struct MinHNode {
	char item;
	unsigned freq;
	struct MinHNode *left, *right;
};

struct MinHeap {
	unsigned size;
  	unsigned capacity;
  	struct MinHNode **array;
};

// Buat nodes
struct MinHNode *newNode(char item, unsigned freq) {
  	struct MinHNode *temp = (struct MinHNode *)malloc(sizeof(struct MinHNode));

  	temp->left = temp->right = NULL;
  	temp->item = item;
  	temp->freq = freq;

  	return temp;
}

// Buat min heap
struct MinHeap *createMinH(unsigned capacity) {
  	struct MinHeap *minHeap = (struct MinHeap *)malloc(sizeof(struct MinHeap));

  	minHeap->size = 0;
  	minHeap->capacity = capacity;
  	minHeap->array = (struct MinHNode **)malloc(minHeap->capacity * sizeof(struct MinHNode *));
  	return minHeap;
}

// Fungsi untuk menukar
void swapMinHNode(struct MinHNode **a, struct MinHNode **b) {
  	struct MinHNode *t = *a;
  	*a = *b;
  	*b = t;
}

// Heapify
void minHeapify(struct MinHeap *minHeap, int idx) {
  	int smallest = idx;
  	int left = 2 * idx + 1;
  	int right = 2 * idx + 2;

  	if (left < minHeap->size && minHeap->array[left]->freq < minHeap->array[smallest]->freq){
  		smallest = left;
  	}
    

  	if (right < minHeap->size && minHeap->array[right]->freq < minHeap->array[smallest]->freq){
  		smallest = right;
  	}
    

  	if (smallest != idx) {
    		swapMinHNode(&minHeap->array[smallest], &minHeap->array[idx]);
    		minHeapify(minHeap, smallest);
  	}
}

// Check jika size 1
int checkSizeOne(struct MinHeap *minHeap) {
  	return (minHeap->size == 1);
}

// Extract min
struct MinHNode *extractMin(struct MinHeap *minHeap) {
  	struct MinHNode *temp = minHeap->array[0];
  	minHeap->array[0] = minHeap->array[minHeap->size - 1];

  	--minHeap->size;
  	minHeapify(minHeap, 0);

  	return temp;
}

// Fungsi insert
void insertMinHeap(struct MinHeap *minHeap, struct MinHNode *minHeapNode) {
  	++minHeap->size;
  	int i = minHeap->size - 1;

  	while (i && minHeapNode->freq < minHeap->array[(i - 1) / 2]->freq) {
    		minHeap->array[i] = minHeap->array[(i - 1) / 2];
    		i = (i - 1) / 2;
  	}
  	minHeap->array[i] = minHeapNode;
}

void buildMinHeap(struct MinHeap *minHeap) {
  	int n = minHeap->size - 1;
  	int i;

  	for (i = (n - 1) / 2; i >= 0; --i)
    	minHeapify(minHeap, i);
}

int isLeaf(struct MinHNode *root) {
  	return !(root->left) && !(root->right);
}

struct MinHeap *createAndBuildMinHeap(char item[], int freq[], int size) {
  	struct MinHeap *minHeap = createMinH(size);
  	int i;
  	for (i = 0; i < size; ++i){
		minHeap->array[i] = newNode(item[i], freq[i]);
  	}

  	minHeap->size = size;
 	buildMinHeap(minHeap);

 	return minHeap;
}

struct MinHNode *buildHuffmanTree(char item[], int freq[], int size) {
  	struct MinHNode *left, *right, *top;
  	struct MinHeap *minHeap = createAndBuildMinHeap(item, freq, size);

  	while (!checkSizeOne(minHeap)) {
    		left = extractMin(minHeap);
    		right = extractMin(minHeap);

    		top = newNode('$', left->freq + right->freq);

    		top->left = left;
    		top->right = right;

    		insertMinHeap(minHeap, top);
  	}
  	return extractMin(minHeap);
}

void printHCodes(struct MinHNode *root, int arr[], int top, int freq2[], int freq3[]) {
	char temp1;
	int temp2;
	int jumlah;
	
	if (root->left) {
		arr[top] = 0;
		printHCodes(root->left, arr, top + 1, freq2, freq3);
  	}
  	if (root->right) {
  		arr[top] = 1;
  		printHCodes(root->right, arr, top + 1, freq2, freq3);
  	}
	if (isLeaf(root)) {
		printf("  %c   | ", root->item);
	    	int i;
  		for (i = 0; i < top; ++i){
  			printf("%d", arr[i]);
		}
  		
		temp1 = root->item;
  		temp2 = freq3[temp1 -65];
  		jumlah = freq2[temp2]*top;
		printf("\nBanyak bit = %d\n", top);
		printf("Banyak Frekuensi = %d\n", freq2[temp2]);
		printf("Total Bit = %d\n\n", jumlah);
		freq2[temp2] = jumlah;	
  	}
}

// Fungsi utama huffman
void HuffmanCodes(char item[], int freq[], int size, int freq3[]) {
  	struct MinHNode *root = buildHuffmanTree(item, freq, size);
  	int arr[MAX_TREE_HT], top = 0;
  	printHCodes(root, arr, top, freq, freq3);
}


int main() 
{ 
	int fd1[2];
	int fd2[2];

	FILE *fp;
    	char c, filename[] = "file.txt";
	pid_t p; 

	if (pipe(fd1)==-1) 
	{ 
		fprintf(stderr, "Pipe Failed" ); 
		return 1; 
	} 
	if (pipe(fd2)==-1) 
	{ 
		fprintf(stderr, "Pipe Failed" ); 
		return 1; 
	} 

	p = fork(); 

	if (p < 0) 
	{ 
		fprintf(stderr, "fork Failed" ); 
		return 1; 
	} 

	// Parent process 
	else if (p > 0) 
	{ 
		close(fd1[0]);

		int i;
		int total = 0;
		int jumlah[23];
		int hasil = 0;
    		int frequency[26] = {0};
    		// Buka file.txt
    		fp = fopen(filename, "r");

    		if (fp == NULL) {
        		printf("File %s tidak dapat dibuka\n", filename);
        		exit(1);
    		}

		// Hitung frekuensi huruf yang ada apabila semua huruf kecil menjadi huruf besar
    		while ((c = fgetc(fp)) != EOF) {
        		if (isalpha(c)) {
            		c = toupper(c);
            		frequency[c-'A']++;
            		total++;
        		}
    		}

    		fclose(fp);
		
		// Kirim frequency melalui pipe1
		write(fd1[1], frequency, sizeof(int)*26); 
		close(fd1[1]); 

		wait(NULL); 

		close(fd2[1]);
		read(fd2[0], jumlah, sizeof(int)*23); 
		
		for(i=0; i<23; i++)
  		{
  			hasil += jumlah[i];
  		}

		
		printf("\nJumlah bit sebelum: %d", total*8);
		printf("\nJumlah bit sesudah: %d\n", hasil);
		close(fd2[0]); 
	} 

	// child process 
	else
	{ 
		close(fd1[1]);
		int total;
		int frequency[26];
		int freq2[23];
		int freq3[26] = {0};
		char arr[26];
		char arr2[23];
		int i;
		int n = 0;
		
		// Menerima input dari pipe1 berupa frequency
		read(fd1[0], frequency, sizeof(int)*26); 

    		printf("\n");
    		
    		// Hilangkan frekuensi huruf yang tidak keluar
    		printf("Frekuensi huruf:\n");
		for(i=0; i<26; i++){
			if(frequency[i] > 0){
				arr2[n] = 'A'+i;
				freq3[i] = n;
				freq2[n] = frequency[i];
				printf("%c: %d\n", arr2[n], freq2[n]);
				n++;
			}		
		}
		printf("\n");
		
  		int size = n;
  		  		
		printf("Char dan Huffman codenya:\n");
  		
  		HuffmanCodes(arr2, freq2, size, freq3);
  		
		close(fd1[0]); 
		close(fd2[0]); 

		write(fd2[1], freq2, sizeof(int)*23); 
		close(fd2[1]); 

		return(0); 
	} 
} 

